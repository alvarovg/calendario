#!/usr/bin/python3

calendar: dict = {}

def add_activity(date: str, time: str, activity: str):
    if date not in calendar:
        calendar[date] = {}
    calendar[date][time] = activity

def get_time(atime):
    """Devuelve todas las actividades para la hora atime como una lista de tuplas"""
    activities: list = []
    for clave in calendar:
        for valor in calendar[clave]:
            if valor == atime:
                actividad = (clave, valor, calendar[clave][valor])
                activities.append(actividad)
    activities.sort()
    return activities
#calendar: dict = {"2023-11-12" : {"09:00" : "Desayuno"}, "2023-11-13" : {"09:00" : "Cena"}}
#get_time("09:00")

def get_all():
    """Devuelve todas las actividade sen el calendario como una lista de tuplas"""
    all: list = []
    for clave in calendar:
        for valor in calendar[clave]:
            actividad = clave, valor, calendar[clave][valor]
            all.append(actividad)
    all.sort()
    return all
#calendar: dict = {"2023-11-12" : {"09:00" : "Desayuno"}, "2023-11-11" : {"21:00" : "Cena"}, "2022-05-05" : {"09:00" : "Levantarse"},
#"2021" : {"11:00":"Desayuno","22:00":"Cena"}}
#get_all()

def get_busiest():
    """Devuelve la fecha con más actividades, y su número de actividades"""
    if len(calendar) >= 1:
        acts: dict = {}
        for clave in calendar:
            numero_act = len(calendar[clave])
            if numero_act in acts:
                existente: list = acts[numero_act]
                existente.append(clave)
                acts[numero_act] = existente
            else:
                acts[numero_act] = [clave]
        busiest = acts[max(acts)]
        busiest_no = max(acts)

        return *busiest, busiest_no
    else:
        return None, 0
#calendar = {}
#get_busiest()


def show(activities):
    for (date, time, activity) in activities:
        print(f"{date}. {time}: {activity}")

def check_date(date):
    """Comprueba si una fecha tiene el formato correcto (aaaa-mm-dd)
    Devuelve True o False"""
    fecha: list = date.split("-")
    if len(fecha)==3 and len(fecha[0])==4 and len(fecha[1])==2 and len(fecha[2])==2 and 0000<=int(fecha[0])<=2100 and 0<int(fecha[1])<=12 and 0<int(fecha[2])<=31:
        return True
    else:
        return False
#check_date("2004-12-05")
def check_time(time):
    """Comprueba si una hora tiene el formato correcto (hh:mm)
    Devuelve True o False"""
    hora = time.split(":")
    if len(hora) == 2 and len(hora[0]) == 2 and len(hora[1]) == 2 and 00<=int(hora[0])<=23 and 00<=int(hora[1])<=59:
        return True
    else:
        return False
#check_time("12:55")
def get_activity():
    """"Pide al usuario una actividad
    En caso de error al introducirla, vuelve a pedirla desde el principio"""
    seguir = True
    while seguir:
        print("Fecha: ", end="")
        fecha = str(input())
        if check_date(fecha) == True:
            print("Hora: ", end="")
            hora = str(input())
            if check_time(hora) == True:
                print("Actividad: ", end="")
                actividad = str(input())
                seguir = False
    return (fecha, hora, actividad)
#tupla: tuple = get_activity()
#add_activity(tupla[0], tupla[1], tupla[2])
def menu():
    print("A. Introduce actividad")
    print("B. Lista todas las actividades")
    print("C. Día más ocupado")
    print("D. Lista de las actividades de cierta hora dada")
    print("X. Terminar")
    asking = True
    while asking:
        option = input("Opción: ").upper()
        if option in ['A', 'B', 'C', 'D', 'X']:
            asking = False
    return option

def run_option(option):
    """Ejecuta el código necesario para la opción dada"""
    if option == "A":
        tupla: tuple = get_activity()
        add_activity(tupla[0], tupla[1], tupla[2])

    if option == "B":
        all: list = get_all()
        for i in range (0, len(all)):
            print(f"{all[i][0]}. {all[i][1]}: {all[i][2]}")

    if option == "C":
        optionC: tuple = get_busiest()
        print(f"El día más ocupado es el {optionC[0]}, con {optionC[1]} actividad(es)")

    if option == "D":
        print("Hora: ", end="")
        time = str(input())
        activities: list = get_time(time)
        for i in range(0, len(activities)):
            print(f"{activities[i][0]}. {activities[i][1]}: {activities[i][2]}")

def main():
    proceed = True
    while proceed:
        option = menu()
        if option == 'X':
            proceed = False
        else:
            run_option(option)

if __name__ == "__main__":
    main()